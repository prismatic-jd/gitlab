#!/bin/bash
# # This bash script iterates over all git tags in the current repository and triggers a pipeline execution for each tag using the GitLab API.

set -eo pipefail
for tag in $(git tag); do
  echo Rebuilding $tag
  # Execute curl request to trigger pipeline execution
  response=$(curl -f --request POST --form token=${CI_JOB_TOKEN} --form ref=${tag} --form "variables[REBUILD_DISABLED]=true" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/trigger/pipeline")

  # Check for error code
  if [ $? -ne 0 ]; then
    echo "Error executing curl request"
    exit $?
  fi

  # Check for error response from GitLab API
  if [ "$(echo "$response" | jq -r '.status')" = "error" ]; then
    echo "Error triggering pipeline for tag $tag"
    exit 1
  fi

  echo "Pipeline triggered for tag $tag"
done
